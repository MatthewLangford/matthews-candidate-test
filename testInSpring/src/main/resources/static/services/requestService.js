angular.module("app").service("requestService", function($http){

    // makes a http request to the server
    this.getList = () =>{
        return $http.get("api/getList");
    }
});
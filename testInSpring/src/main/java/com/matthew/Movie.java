package com.matthew;

public class Movie {
	
	private String year;
	
	private String actress;
	
	private String title;

	
	public Movie(String year, String actress, String title) {
		this.year = year;
		this.actress = actress;
		this.title = title;
	}
	
	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getActress() {
		return actress;
	}

	public void setActress(String actress) {
		this.actress = actress;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
}

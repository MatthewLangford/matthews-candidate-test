package com.matthew;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class TestInSpringApplication {
		
	public static void main(String[] args) {
		SpringApplication.run(TestInSpringApplication.class, args);
	}
	
	
	@GetMapping("/api/getList")
	public @ResponseBody List<Movie> getList(){
		return makeMovieListFromCsv(); 
	}
	
	
	public List<Movie> makeMovieListFromCsv(){
		// create a list of the movies to be filled in by the cssv data
		List<Movie> movies = new ArrayList<>();
		
		try {
			// get the csv from resources
			File csv = new ClassPathResource("sample.csv").getFile();

			// read in the csv with scanner
			Scanner movieScanner = new Scanner(csv);
			
			// loop through the read in csv creating movies out of the lines then add them to the list of movies
			while(movieScanner.hasNextLine())  {
				String[] next = movieScanner.nextLine().replaceAll("\"", "").split(",");
				
				movies.add(new Movie(next[0], next[1], next[2]));

			}
			
			// close resource and return list of movies
			movieScanner.close();
			return movies;
		} catch (IOException e) {
			e.printStackTrace();
			Movie error = new Movie("", "Something went wrong, please try again later", "");
			movies.add(error);
			return movies;
		}
	}
}

const express = require('express'),
    app = module.exports = express(),
    bodyParser = require('body-parser'),
    mainServerCtrl = require('./mainServerCtrl');

app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));

// sends get request from the front to the mainServerCtrl and calls the getList method there
app.get('/api/getList', mainServerCtrl.getList);

app.listen(3050, ()=>{
console.log("listening on 3050")
});
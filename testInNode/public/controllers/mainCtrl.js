angular.module("app").controller("mainCtrl", function($scope, $window, requestService){

    requestService.getList()
        .then(response => {
            $scope.movies = response.data
        });

    $scope.alertData = (person) =>{
        // use this for the data in json format
        //$window.alert(JSON.stringify(person));

        $window.alert(alertSetup(person))
    };

    let alertSetup = (person) =>{
        let personString = "";
        for(let field in person){
            if(person.hasOwnProperty(field) && field !== "$$hashKey") {
                personString += person[field] + " ";
            }
        }
        return personString;
    }
});
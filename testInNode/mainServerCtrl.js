const csv = require("fast-csv"),
      fs = require("fs");

module.exports ={
    getList : (req, res, next) =>{
       let objects = [];

       fs.createReadStream("resources/academy_award_actresses.csv")
       .pipe(csv())
       .on("data", data =>{
        objects.push({
            "year": data[0],
            "actress" : data[1],
            "title" : data[2]
            })
       })
       .on("end", ()=>{
            res.send(objects);
       });
    }
};